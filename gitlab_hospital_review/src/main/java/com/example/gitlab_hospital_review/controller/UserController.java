package com.example.gitlab_hospital_review.controller;

import com.example.gitlab_hospital_review.domain.Response;
import com.example.gitlab_hospital_review.domain.dto.UserDto;
import com.example.gitlab_hospital_review.domain.dto.UserJoinRequest;
import com.example.gitlab_hospital_review.domain.dto.UserJoinResponse;
import com.example.gitlab_hospital_review.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping("/join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        UserDto userDto = userService.join(userJoinRequest);
        return Response.success(new UserJoinResponse(userDto.getUserName(), userDto.getEmail()));
    }
}
