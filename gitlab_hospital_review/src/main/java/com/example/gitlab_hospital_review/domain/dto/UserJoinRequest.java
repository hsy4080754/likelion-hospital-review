package com.example.gitlab_hospital_review.domain.dto;

import com.example.gitlab_hospital_review.domain.User;
import com.example.gitlab_hospital_review.domain.UserRole;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class UserJoinRequest {
    private String userName;
    private String password;
    private String email;

    public User toEntity(String password){
        return User.builder()
                .userName(this.userName)
                .password(password)
                .emailAddress(this.email)
                .role(UserRole.USER)
                .build();
    }
}
