package com.example.gitlab_hospital_review.domain;

public enum UserRole {
    ADMIN, USER;
}
