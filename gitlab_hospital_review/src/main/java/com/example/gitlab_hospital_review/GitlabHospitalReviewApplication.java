package com.example.gitlab_hospital_review;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GitlabHospitalReviewApplication {

    public static void main(String[] args) {
        SpringApplication.run(GitlabHospitalReviewApplication.class, args);
    }

}
